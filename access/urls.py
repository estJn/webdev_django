from django.urls import path
from . import views

urlpatterns = [
  path("", views.index, name = "index"),
  path("open/", views.open, name="open"),
  path("close/", views.close, name="close"),
]
