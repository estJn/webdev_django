from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import messages

def index(request):
    return render(request, 'access/index.html')

def open(request):
    if (request.method).upper() == "POST":
        # print("post")
        guest_id = request.POST['invitation_id']
        guest_pw = request.POST['invitation_pw']
        guest = authenticate(username=guest_id, password=guest_pw)
        print(guest_pw, " : ", guest)

        if guest is not None:
            login(request, guest)
            # return render(request, 'access/invitation_success.html')
            # return reverse(invitationView.home)
            return redirect('home')
        else:
            messages.info(request, "Access Denied")
            return redirect('index')
            # return render(request, 'access/invitation_error.html')

    else:
        return redirect('index')

def close(request):
    logout(request)
    return redirect('index')
