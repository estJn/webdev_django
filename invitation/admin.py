from django.contrib import admin

# Register your models here.
from .models import GuestRSVP

admin.site.register(GuestRSVP)
