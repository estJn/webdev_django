from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.template.context_processors import csrf
from .forms import RsvpForm
from .models import GuestRSVP


# Create your views here.

@login_required(login_url='/')
def home(request):
    context = {}
    return render(request, 'invitation/home.html', context)

@login_required(login_url='/')
def reception(request):
    return render(request, "invitation/eventInfo.html")

@login_required(login_url='/')
def dress(request):
    return render(request, "invitation/dressInfo.html")

@login_required(login_url='/')
def rsvp(request):
    context = {}
    context.update(csrf(request))

    if request.method == 'GET':
        context['form']= RsvpForm()
        form = RsvpForm()
    else:
        form = RsvpForm(request.POST)

        if form.is_valid():
            guestRSVP = GuestRSVP (
            # guest_name = form.cleaned_data['guest_name'],
            guest_name = request.user.get_username(),
            guest_attend = form.cleaned_data['guest_attend'],
            guest_entree = form.cleaned_data['guest_entree'],
            guest_comment = form.cleaned_data['guest_comment'],
            # guest_plus = form.cleaned_data['guest_plus'],

            # plus_name = form.cleaned_data['plus_name'],
            # plus_entree = form.cleaned_data['plus_entree'],
            # plus_comment = form.cleaned_data['plus_comment'],
            post_date = timezone.now(),
            )
            guestRSVP.save()

            return render(request, "invitation/rsvp_sent.html")
        else:
            # form is invalid
            context['form'] = form

    return render(request, "invitation/rsvp.html", context)

    # return render(request, "invitation/rsvp.html", {'form': form})
    # return render(request, "invitation/rsvp.html")
