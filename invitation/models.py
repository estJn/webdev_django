from django.db import models
from invitation.choices import *
# Create your models here.

class GuestRSVP(models.Model):
    guest_name = models.CharField(null=False, max_length=20, default='guest')
    guest_attend = models.CharField(choices=YN_CHOICES, default='Y', max_length=40)
    guest_entree = models.CharField(choices=NUM_CHOICES, default='N/A', max_length=40)
    guest_comment = models.CharField(max_length=200)

    post_date = models.DateTimeField(null=True, blank=True)
