from django.urls import path
from . import views

urlpatterns = [
  path('', views.home, name='home'),
  path('event/', views.reception, name = 'event'),
  path('dress/', views.dress, name = 'dress'),
  path('rsvp/', views.rsvp, name='rsvp'),
  # path('rsvp/sent', views.rsvp_sent, name='rsvp_sent'),
]
