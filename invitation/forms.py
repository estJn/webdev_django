from django import forms
from invitation.choices import *

class RsvpForm(forms.Form):
    # guest_name = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder':'first name'}))
    guest_attend = forms.ChoiceField(choices = YN_CHOICES, label="", widget=forms.RadioSelect(), required=True)
    guest_entree = forms.ChoiceField(choices = NUM_CHOICES, label="Choice ", widget=forms.RadioSelect(), required=False)
    guest_comment = forms.CharField(required=False, max_length=200, label="", widget=forms.Textarea(attrs={'placeholder':'BLAB'}))
